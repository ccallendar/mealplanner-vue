import {Time} from "../collections/time";


Meteor.methods({
  updateTime() {
    Time.upsert("currentTime", { $set: { time: new Date() }});
  }
});