import {Recipes, Meals} from "../collections/index";

Meteor.methods({
  addRecipe(recipe) {
    let result = { success: false, msg: null, rv: null };
    if (recipe && recipe.title) {
      if (recipe._id) {
        result.rv = Recipes.upsert(recipe._id, { $set: recipe});
      } else {
        delete recipe._id; // remove blank id field
        result.rv = Recipes.insert(recipe);
      }
    }
    return result;
  },
  addMeal(meal) {

  }
});