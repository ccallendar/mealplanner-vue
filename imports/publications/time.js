import {Time} from "../collections/time";

Meteor.publish("Time", () => {
  return Time.find({});
});