import {Meals, Recipes} from "../collections/index";

export default class Publications {
  static initialize() {
    Meteor.publish("Meals", (query) => {
      return Meals.find(query || {});
    });

    Meteor.publish("Recipes", (query) => {
      console.log("Publish Recipes", query);
      if (query) {
        if (query instanceof Object) {
          return Recipes.find(query);
        } else if (typeof query === "string") {
          // Assume id
          return Recipes.find(query, {limit: 1});
        } else {
          return this.ready();
        }
      }
      // Return all recipes
      return Recipes.find({});
    });
  }
}