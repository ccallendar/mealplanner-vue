export const Meals = new Mongo.Collection("Meals");

export const Recipes = new Mongo.Collection("Recipes");

if (Meteor.isClient) {
  Meteor.Collections = {};
  Meteor.Collections.Meals = Meals;
  Meteor.Collections.Recipes = Recipes;
}