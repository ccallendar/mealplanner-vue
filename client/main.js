import Vue from 'vue';
import VueMeteorTracker from 'vue-meteor-tracker';   // here!
import './main.html';
import './main.less';
import routerFactory from "./routes";
import AppLayout from '/client/layouts/AppLayout.vue';

Vue.use(VueMeteorTracker);

Meteor.startup(() => {
  const router = routerFactory.create();
  window.app = new Vue({
    el: '#app',
    router,
    render: h => h(AppLayout),
  });
});
