import { RouterFactory, nativeScrollBehavior } from 'meteor/akryum:vue-router2';

import About from '/client/pages/About.vue';
import Home from '/client/pages/Home.vue';
import RecipesList from '/client/pages/RecipesList.vue';
import RecipeDisplay from '/client/pages/RecipeDisplay.vue';
// import RecipeEdit from '/client/pages/RecipeEdit.vue';
import NotFound from '/client/pages/NotFound.vue';

// Create router instance
const routerFactory = new RouterFactory({
  mode: 'history',
  scrollBehavior: nativeScrollBehavior
});

RouterFactory.configure(factory => {
  factory.addRoutes([
    {
      path: '/',
      name: 'home',
      component: Home,
    },
    {
      path: '/home',
      redirect: '/'
    },
    {
      path: '/about',
      name: 'about',
      component: About,
    },
    // {
    //   path: '/recipes/new',
    //   name: 'recipeNew',
    //   component: RecipeEdit,
    // },
    // {
    //   path: '/recipes/edit/:id',
    //   name: 'recipeForm',
    //   component: RecipeEdit,
    //   props: (route) => ({recipeId: route.params.id}),
    // },
    {
      path: '/recipes/view/:id',
      name: 'recipeDisplay',
      component: RecipeDisplay,
      props: (route) => ({recipeId: route.params.id}),
    },
    {
      path: '/recipes',
      name: 'recipesList',
      component: RecipesList,
    },
    {
      path: '*',
      component: NotFound,
      priority: -1,
    },
  ]);
});

export default routerFactory;