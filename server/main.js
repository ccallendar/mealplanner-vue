import { Meteor } from 'meteor/meteor';
import _ from 'lodash';
import {date as fakeDate, image, internet, random, helpers} from 'faker';
import "../imports/collections/index";
import "../imports/methods/index";
import {Time} from "../imports/collections/time";
import {Recipes} from '../imports/collections';
import "../imports/publications/time";
import "../imports/methods/update-time";
import Publications from "../imports/publications/publications";



Meteor.startup(() => {
  // code to run on server at startup
  Meteor.call("updateTime");
  Time.insert({ time: new Date() });
  console.log("Server started");

  // Recipes.remove({});
  const count = Recipes.find({}).count();
  if (count === 0) {
    // Generate fake data
    console.log("Creating fake data");
    _.times(50, () => {
      const title = random.words();
      const date = fakeDate.recent();
      const url = internet.url();
      const photoUrl = image.avatar();
      const rating = Math.round(Math.random() * 5);
      const notes = random.words();
      Recipes.insert({
        title,
        dateAdded: date,
        dateUpdated: date,
        url,
        photoUrl,
        rating,
        notes,
      });
    });
  } else {
    console.log(`Found ${count} recipes`);
  }

  // Create publications
  Publications.initialize();
});
